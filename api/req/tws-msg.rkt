#lang racket
;;#lang typed/racket/no-check

(provide
 tws-msg)

(require
 bitsyntax
 (only-in "../../comm/types.rkt"
          Msg message))

(define-syntax (tws-msg stx)
  (syntax-case stx (::)
    ([_ fields ...]
     #'(message (bit-string->bytes
                 (bit-string fields ...))))))
