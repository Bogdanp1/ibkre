#lang typed/racket/no-check
;;#lang typed/racket

(provide
 tws-msg
 tws-msg->bytes
 tws-msg->msg)

(require
 bitsyntax
 (only-in "../comm/types.rkt"
          Msg message))

#| Two levels of encoding an out bound message.
The lowest layer encodes string fields into a TWS null delimited
msg of bytes by converting the strings to UTF8 encoded bytes.

The next level encodes each specific type to a string.  e.g. An Integer or an Action.
|#


#| Form a TWS message body (it is missing the prefix length) 
delimited message from string fields. |#
(define-syntax tws-field
  (syntax-rules ()
    ;; #f to signal it is being used as a formatter.
    [(_ #f val)
     (let ((s-val (string->bytes/utf-8 (format "~a" val))))
       (bit-string (s-val :: binary) 0))]))

(define-syntax tws-msg
  (syntax-rules ()
    ((_ f0 ...)
     (bit-string (f0 :: (tws-field)) ...))))

(: tws-msg->bytes (-> BitString Bytes))
(define tws-msg->bytes bit-string->bytes)

(: tws-msg->msg (-> BitString Msg))
(define (tws-msg->msg bs)
  (message (tws-msg->bytes bs)))

#| Encode a Message from various field types where each field type has a 
custom encoder to encode the field as a string. |#

