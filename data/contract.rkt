#lang typed/racket/no-check

(provide
 (struct-out ComboLeg)
 (struct-out DeltaNeutralContract)
 (struct-out ContractDescription) contract-description-template
 (struct-out Contract)
 (struct-out ContractDetails)
 (struct-out ContractDetailsEnd))

(require
 (only-in "field.rkt"
          Action SecurityIdType
          SecurityType TickerId
          Market-Datatype)
 (only-in "order.rkt"
           SecurityId))

(define-type Position (U 'SAME_POS 'OPEN_POS 'CLOSE_POS 'UNKNOWN_POS))


(define-type Security (U 'stk 'fut 'cash 'bond 'cfd 'fop 'war
                         'iopt 'fwd 'bag 'ind 'bill 'fund 'fixed
                         'slb 'news 'cmdty 'bsk 'icu 'ics))


(define-type Right (U 'call 'put))

(struct Hdr ([msg-id : Integer]
             [version : Integer]) #:transparent)

(struct ComboLeg ([conId : Integer]
                  [ratio : Integer]
                  [action : Action]
                  [exchange : String]
                  [openClose : Integer] ;; LegOpenClose enum values
                  ;; for stock legs when doing short sale
                  [shortSaleSlot : Integer] ;; default to 0
                  [designatedLocation : String]
                  [exemptCode : Integer] ;; default to -1
                  ) #:transparent)

(struct DeltaNeutralContract ([conId : Integer]
                              [delta : Float]
                              [price : Float]) #:transparent)

(struct Contract ([conId                        : Integer] ;; default to 0
                  [symbol                       : String]
                  [secType                      : SecurityType]
                  [lastTradeDateOrContractMonth : String]
                  [strike                       : Float] ;; default to 0.0
                  [right                        : String]
                  [multiplier                   : String]
                  [exchange                     : String]
                  ;; Pick actual exchange that contract trades on.
                  ;; DO NOT SET TO SMART.
                  [primaryExchange              : String]
                  [currency                     : String]
                  [localSymbol                  : String]
                  [tradingClass                 : String]
                  [includeExpired               : Boolean]
                  [secIdType                    : (Option SecurityIdType)] ;; CUSIP, SEDOL, ISIN, RIC
                  [secId                        : String]
                  ;; Combos
                  ;; Received in open order 14 and up for all combos
                  [comboLegsDescrip             : String]
                  [comboLegs                    : (Listof ComboLeg)]  ;; type : ListOf ComboLeg
                  [deltaNeutral                 : (Option DeltaNeutralContract)]
                  ) #:transparent)

(struct ContractDetails ([contract           : Contract]
                         [marketName         : String]
                         [minTick            : String]
                         [orderTypes         : String]
                         [validExchanges     : String]
                         [priceMagnifier     : Integer] ;; default 0
                         [underConId         : Integer] ;; default to 0
                         [longName           : String]
                         [contractMonth      : String]
                         [industry           : String]
                         [category           : String]
                         [subcategory        : String]
                         [timeZoneid         : String]
                         [tradingHours       : String]
                         [evRule             : String]
                         [evMultiplier       : Float]   ;; def 0.0
                         [mdSizeMultiplier   : Integer] ;; def 0
                         [aggGroup           : Integer] ;;def 0
                         [undefSymbol        : String]
                         [underSecType       : SecurityType]
                         [marketRuleIds      : String]
                         [secIdList          : (Listof SecurityId)]
                         [realExpirationDate : String]
                         [lastTradeTime      : String]
                         [bond               : (Option BondDetails)]
                         ) #:transparent)

  (struct BondDetails ([cusip              : String]
                       [ratings            : String]
                       [descAppend         : String]
                       [bondType           : String]
                       [couponType         : String]
                       [callable           : Boolean]
                       [putable            : Boolean]
                       [coupon             : Integer] ;; def 0
                       [convertable        : Boolean] ;; #f
                       [maturity           : String]
                       [issueDate          : String]
                       [nextOptionDate     : String]
                       [nextOptionType     : String]
                       [nextOptionPartial  : Boolean] ;; def #f
                       [notes              : String]) #:transparent)

(struct ContractDetailsEnd ([tic : TickerId]) #:transparent)

(struct ContractDescription ([contract : Contract]
                             [derivativeSecTypes : (Listof String)]) #:transparent)


(: contract-description-template (-> Integer String SecurityType String String
                                     Contract))
(define (contract-description-template conid sym sec-t exch curr)
  (Contract conid sym sec-t "" 0.0 "" "" "" exch curr "" "" #f
            #f "" "" '() #f))
