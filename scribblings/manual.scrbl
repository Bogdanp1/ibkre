#lang scribble/manual

@(require racket/sandbox
          scribble/example)

@(define my-evaluator
   (parameterize ([sandbox-output 'string]
                  [sandbox-error-output 'string]
                  [sandbox-memory-limit 200])
     (make-evaluator 'racket/base)))

@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]
 	 	
@title{Using Ibkre}
This Intro / First Tutorial shows how to use the Ibkre Library to interact simply and efficiently with the IBK API.

The IBK API is an asynchronous API.  As a consequence a client of the IBK API fires requests from one thread of execution, and some unbounded time later responses are received from the IBK server on another thread of execution in your program.
Efficient utililization of the API suggests the client code is to be written concurrently where numerous API requests and responses are all happening at-the-same-time.
Howeer, concurrent, asynchronous code can be hard to wrangle when everything-is-happening-at-once.  

There are various coding paradigms one can use to prevent melting your brain when coding at the lowest levels with an asynchronous API.
Ibkre takes an event based, reactive, Actor focused approach to writing async, concurrent programs which makes using the IPK API simple.

Ibkre Library itself uses the wonderful Syndicate Actor Library by Tony Garnock-Jones.

After a brief background introduction on Actors, we will get to our first quick example of code.

@section{Conceptual Picture}

So let's break down what an event based, reactive, actor based system is all about.

@subsection{Actors}

Actors are a way of structuring code in some programming language similar how one may structure their code in some languages via Classes and Objects.
There is nothing special about Actors, in fact one may write an Actor system in pretty much any language.  

The essential characteristics of the Actor model are:
@itemlist[
 @item{Actor stores internal state much like a Class/Object.}
 @item{An Actor receives and acts on messages received from other Actors and process those messages one message at a time.}
 @item{An Actor may send messages to other Actors.}
 @item{An Actor may create other Actors.}
 @item{A program of Actors are all sending, receiving and processing messages all at the same time.}
 ]

@subsection{Event Based, Reactive}

A typical program in say Java or Python is all about calling methods and/or procedures in some sequence.  It sort of pushes itself along.  Some top level "main" calls something which calls something, which calls something ...
The program ends when the top level "main" returns.

A reactive program inverts the logic, it sort of pulls itself along, by reacting to a sequence of events.  A reactive program ends when there are no more events.  Events are the messages in our Actor Model.

Our Actors are independent threads of execution that send and react to message events.
For example, we might have several Actors responding to incoming IBK Tick data concerning various underlying instruments, while another Actor is placing an order, while yet another Actor is monitoring margin requirements, all running at the same time.
