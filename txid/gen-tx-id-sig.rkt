#lang typed/racket/no-check

(provide
 gen-tx-id^)

(require
 (only-in "../data/field.rkt"
          RequestId
          TickerId))

(define-signature gen-tx-id^
  ([gen-tid : (-> TickerId)]
   [gen-rid : (-> RequestId)]))
